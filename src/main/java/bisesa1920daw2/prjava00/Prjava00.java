/*
 * Biel Serrano Sánchez, DAW2
 * 2019-09-27
 */
package bisesa1920daw2.prjava00;

import java.net.*;

/**
 * Main Class per prjava00
 * @author biels
 */
public class Prjava00 {
    public static void main (String[] args) {
        System.out.println("Hola món");
        System.out.println("Versió 1.0 del projecte prjava00");
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String ipAddr = addr.getHostAddress();
            String hostname = addr.getHostName();
            System.out.println("hostname=" + hostname);
            System.out.println("Adreça IP: " + ipAddr);
            System.out.println("Nom de l'usuari: " 
                    + System.getProperty("user.name"));
            System.out.println("Carpeta Personal: " 
                    + System.getProperty("user.home"));
            System.out.println("Sistema Operatiu: " 
                    + System.getProperty("os.name"));
            System.out.println("Versió OS: " 
                    + System.getProperty("os.version"));
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
